package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

const MemeApiUrl = "https://meme-api.herokuapp.com/gimme"

type user struct {
	username  string
	firstName string
	lastName  string
}

type ApiResponse struct {
	PostLink  string `json:"postLink"`
	Subreddit string `json:"subreddit"`
	Title     string `json:"title"`
	Url       string `json:"url"`
}

func init() {
	fmt.Println("Loading environments...")
	err := godotenv.Load()
	handleErr(err)
}

func handleErr(err error) {
	if err != nil {
		log.Panic(err)
	}
}

func getMemeUrl() ApiResponse {
	resp, err := http.Get(MemeApiUrl)
	handleErr(err)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var m ApiResponse
	err = json.Unmarshal(body, &m)
	handleErr(err)
	return m
}

func main() {
	var BotToken = os.Getenv("BOT_TOKEN")

	bot, err := tgbotapi.NewBotAPI(BotToken)
	handleErr(err)

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message Updates
			continue
		}
		if !update.Message.IsCommand() { // ignore any non-command Messages
			continue
		}

		// Create a new MessageConfig. We don't have text yet,
		// so we should leave it empty.
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")

		// Extract the command from the Message.
		switch update.Message.Command() {
		case "start":
			msg.Text = "/getmeme\n/status"
		case "help":
			msg.Text = "/getmeme\n/status"
		case "getmeme":
			url := getMemeUrl().Url
			fmt.Println(url)
			msg.Text = url
		case "status":
			msg.Text = "I'm ok."
		default:
			msg.Text = "I don't know that command"
		}

		usr := user{
			username:  update.Message.From.UserName,
			firstName: update.Message.From.FirstName,
			lastName:  update.Message.From.LastName,
		}

		log.Printf("[%s] %s", usr, update.Message.Text)

		//msg = tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
		//msg.ReplyToMessageID = update.Message.MessageID

		_, err = bot.Send(msg)
		handleErr(err)
	}
}
